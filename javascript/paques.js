
/*
  paques.js
  Date de Pâques et du mercredi des Cendres, pour l'année courante et pour l'année suivante.
*/

function div(a, b)
{
  return Math.trunc(a / b)
}

function EasterNumber(aYear)
{
  var g = 0;
  var c = 0;
  var e = 0;
  var h = 0;
  var k = 0;
  var p = 0;
  var q = 0;
  var i = 0;
  var b = 0;
  var j = 0;
  
  g = aYear % 19;
  c = div(aYear, 100);
  e = div(8 * c + 13, 25);
  h = (19 * g + c - div(c, 4) - e + 15) % 30;
  k = div(h, 28);
  p = div(29, h + 1);
  q = div(21 - g, 11);
  i = (k * p * q - 1) * k + h;
  b = div(aYear, 4) + aYear;
  j = (b + i + 2 + div(c, 4) - c) % 7;
  
  return 28 + i - j;
}

Date.prototype.addDays = function(aDayNumber) /* https://stackoverflow.com/a/563442/18595765 */
{
  var lDate = new Date(this.valueOf());
  lDate.setDate(lDate.getDate() + aDayNumber);
  return lDate;
}

function DateToWords(aDate) {
  const cMonthName = ["janvier", "février", "mars", "avril"];
  return aDate.getDate().toString().concat(" ", cMonthName[aDate.getMonth()]);
}

function WriteEasterDate(aYear)
{
  var lEasterNumber = EasterNumber(aYear);
  var lMonth = 0;
  var lDay = 0;

  if (lEasterNumber <= 31)
  {
    lMonth = 3;
    lDay = lEasterNumber;
  }
  else
  {
    lMonth = 4;
    lDay = lEasterNumber - 31;
  }

  var lEaster = new Date(aYear, lMonth - 1, lDay);

  document.write(
    "<li>En ", aYear,
    ", le dimanche de Pâques est le ", DateToWords(lEaster),
    ". Le premier jour du Carême est le ", DateToWords(lEaster.addDays(-46)),
    ".</li>"
  );
}

var lYear = new Date().getFullYear()

document.write("<ul>");

/* Écrire la date de Pâques et du mercredi des Cendres pour l'année courante. */
WriteEasterDate(lYear);

/* Et pour l'année suivante. */
WriteEasterDate(lYear + 1);

document.write("</ul>");
