# Easter date

Date of Easter sunday, in Julian and Gregorian calendars, in several programming languages.

See also this [demo using LaTeX and Lua](https://codeberg.org/rchastain/paques).
