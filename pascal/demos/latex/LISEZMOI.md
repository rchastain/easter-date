# Easter

Exemple d'utilisation de l'unité *EasterDate*.

Génération d'un document LaTeX contenant la date de Pâques et des fêtes mobiles pour l'année courante.

Le document peut être produit dans plusieurs langues. Le programme utilise l'unité [LightweightTranslationManager](https://www.lazarusforum.de/viewtopic.php?t=11928).
